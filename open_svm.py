import joblib
from skimage import data, color, feature
import skimage
from skimage import transform
import numpy as np
import matplotlib.pyplot as plt
import bens_hog
import os


def merge_boxes(indices):
    avg_x = 0.0
    avg_y = 0.0
    for x in indices:
        avg_x += x[1]
        avg_y += x[0]
    try:
        avg_x = avg_x/len(indices)
        avg_y = avg_y/len(indices)
    except ZeroDivisionError:
        return -1,-1
    return avg_x,avg_y


":returns an (x,y) array of slices"
def sliding_window(img, patch_size,
                   istep=2, jstep=2, scale=1.0):
    Ni, Nj = (int(scale * s) for s in patch_size)
    for i in range(0, img.shape[0] - Ni, istep):
        for j in range(0, img.shape[1] - Ni, jstep):  # TODO: warning
            patch = img[i:i + Ni, j:j + Nj]
            if scale != 1:
                patch = transform.resize(patch, patch_size)
            yield (i, j), patch

def check_true(indices):
    max_distance = 30
    indies = indices[0][0],indices[0][1]
    for patch in indices:
        # print(patch, indies)

        if  abs(indies[1] - patch[1]) > max_distance or abs(indies[0] - patch[0]) > max_distance:
            return False
        indies = patch[0], patch[1]

    return True


def dynamic_window():
    test_image = skimage.transform.rescale(test_image, 0.5)


def test(model, face_patch_size,image):

    # test_image = skimage.data.astronaut()
    test_image = plt.imread(image)
    test_image = bens_hog.to_gray(test_image)
    # test_image = skimage.color.rgb2gray(test_image)
    test_image = skimage.transform.rescale(test_image, 0.5)
    # test_image = test_image[:160, 40:180]


    # plt.imshow(test_image, cmap='gray')
    # plt.axis('off')

    indices, patches = zip(*sliding_window(test_image,face_patch_size))
    print(face_patch_size)
    # patches_hog = np.array([bens_hog.is_like_death(patch, 62, 47) for patch in patches])
    patches_hog = np.array([feature.hog(patch) for patch in patches])
    print(patches_hog.shape)
    labels = model.predict(patches_hog)

    print(labels.sum())


    fig, ax = plt.subplots()

    ax.imshow(test_image, cmap='gray')
    ax.axis('off')

    Ni, Nj = face_patch_size
    indices = np.array(indices)
    # print(indices[labels == 1])
    if len(indices) <=0:
        return False
    avg_x, avg_y = merge_boxes(indices[labels == 1])

    valid = check_true(indices)
    print(valid)

    ax.add_patch(plt.Rectangle((avg_x, avg_y), Nj, Ni, edgecolor='blue',
                                   alpha=0.3, lw=2, facecolor='none'))
    plt.waitforbuttonpress()

    for i, j in indices[labels == 1]:
        ax.add_patch(plt.Rectangle((j, i), Nj, Ni, edgecolor='red',
                                   alpha=0.3, lw=2, facecolor='none'))
    plt.waitforbuttonpress()
    plt.close()

    return valid


def main():
    model = joblib.load('model.pickle')
    face_size = (62, 47)

    #test(model,face_size,'daniel.jpeg')


    path = r'faces/img_align_celeba'
    count = 0
    images_count = 100
    for image in os.listdir(path)[:images_count]:
        if test(model,face_size, path+'\\'+image):
            count += 1

    print("valid is ",count,"from ", images_count)



if __name__ == '__main__':
    main()
