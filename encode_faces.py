import face_recognition
import pickle
import cv2
import os


# path to input directory of faces + images
dataset = "test"
# path to serialized db of facial encodings
encoding_save = "daniel.pickle"
# face detection model to use: either `hog` or `cnn`
# TODO: temperraly used BB by faceRec libary
detection_method = "hog"


print("[INFO] quantifying faces...")
imagePaths = list(os.listdir(dataset))
data = []


def main():
	# loop over the image paths
	for (i, imagePath) in enumerate(imagePaths):
		# load the input image and convert it from RGB (OpenCV ordering)
		# to dlib ordering (RGB)
		print("[INFO] processing image {}/{}".format(i + 1, len(imagePaths)))
		imagePath = dataset+'/'+imagePath
		print(imagePath)
		image = cv2.imread(imagePath)
		rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

		# detect the (x, y)-coordinates of the bounding boxes
		# corresponding to each face in the input image
		boxes = face_recognition.face_locations(rgb, model=detection_method)  # bounding box

		# compute the facial embedding for the face
		encodings = face_recognition.face_encodings(rgb, boxes)

		# build a dictionary of the image path, bounding box location,
		# and facial encodings for the current image
		d = [{"imagePath": imagePath, "loc": box, "encoding": enc} for (box, enc) in zip(boxes, encodings)]
		data.extend(d)
		print(d)
		# print(data)
	d = [{"users_list":['b','n']}]
	data.extend(d)
	# dump the facial encodings data to disk
	print("[INFO] serializing encodings...")
	f = open(encoding_save, "wb")
	f.write(pickle.dumps(data))
	f.close()


if __name__ == '__main__':
	main()
