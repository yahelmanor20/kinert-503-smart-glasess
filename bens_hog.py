# from PIL import Image
import numpy as np
import math
import time


def to_gray(img):
	"""
	the function make a 3 channels image(RGB) into grey scale of one channel
	"""
	# The axis=2 argument tells numpy.mean() to average values across all three color channels. (axis=0 would average across pixel rows and axis=1 would average across pixel columns.)
	return np.mean(img, axis=2)


def is_like_death(im,width,height,size_of_cell=8):
	"""
	find the gradient of each pixel
	make cells from the image and put in each cell the gradient of it
	"""
	cells = []
	cell = [x-x for x in range(10)]
	for Y in range(0,height,size_of_cell):
		for X in range(0,width,size_of_cell):
			for y in range(Y,Y + size_of_cell):
				for x in range(X,X + size_of_cell):
					if x < width and y < height:
						calculate_cell(im,cell,x,y,width,height)
			cells.append(cell)
	return np.array(cells)


def calculate_cell(im, cell, x, y, width, height):
	"""divide the picture into cells
	each cell holds a couple of gradients(magnitude+orientation)"""
	x1 = 0
	x2 = 0
	y1 = 0
	y2 = 0
	if(x+1<width):
		x1 = im[x + 1, y]
	if(x-1>0):
		x2 = im[x - 1, y]
	if(y-1>0):
		y2 = im[x, y - 1]
	if(y+1<height):
		y1 = im[x, y + 1]
	X1 = x1-x2
	Y1 = y1-y2
	g = int(((Y1**2)+(X1**2))**0.5)  # calculating magnitude
	d = float((math.atan2(Y1,X1)*180))/(math.pi)  # calculating orientation - atan2 returns radians - turning it to degrees by multipling it by 180 and then divide it by PI
	if d >= 0 and d <= 20:
		cell[1] = (g * ((d)/20.0)) + cell[0]
		cell[2] = (g * ((20-d)/20.0)) + cell[1]
	if d >= 20 and d <= 40:
		cell[1] = (g * ((d - 20)/40.0)) + cell[1]
		cell[2] = (g * ((40-d)/40.0)) + cell[2]
	if d >= 40 and d <= 60:
		cell[2] = (g * ((d - 40)/60.0)) + cell[2]
		cell[3] = (g * ((60-d)/60.0)) + cell[3]
	if d >= 60 and d <= 80:
		cell[3] = (g * ((d - 60)/80.0)) + cell[3]
		cell[4] = (g * ((80-d)/80.0)) + cell[4]
	if d >= 80 and d <= 100:
		cell[4] = (g * ((d - 80)/100.0)) + cell[4]
		cell[5] = (g * ((100-d)/100.0)) + cell[5]
	if d >= 100 and d <= 120:
		cell[5] = (g * ((d - 100)/120.0)) + cell[5]
		cell[6] = (g * ((120-d)/120.0)) + cell[6]
	if d >= 120 and d <= 140:
		cell[6] = (g * ((d - 120)/140.0)) + cell[6]
		cell[7] = (g * ((140-d)/140.0)) + cell[7]
	if d >= 140 and d <= 160:
		cell[7] = (g * ((d - 140)/160.0)) + cell[7]
		cell[8] = (g * ((160-d)/160.0)) + cell[8]
	if d >= 160 and d <= 180:
		cell[8] = g + cell[8]
