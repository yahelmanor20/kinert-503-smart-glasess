import PIL
from PIL import Image
import numpy as np
import math
import matplotlib.pyplot as plt
def avg(v):
	sum = 0
	cnt = 0
	for i in v:
		sum = sum + i
		cnt = cnt + 1
	return float(sum) / cnt
def to_grey(im,width,height):
	for x in range(0,width):
		for y in range(0,height):
			r,g,b = im.getpixel((x,y))	
			avg = int((r+g+b)/3)
			t=(avg,avg,avg)
			im.putpixel((x,y),t)
def SVM(im,hog,width,height,size_of_cell=4,size_of_block=4,pyramid=3):
	list_of_img = [im]
	img_width, img_height = im.size
	cnt = 0
	found = True
	for y in range(0,height):
		for x in range(0,width):
			##creating a block of cells. each cells hold size_of_cell*size_of_cell of gradients.			
			send_to_svm = []
			for y1 in range( y, y + size_of_block):
				for x1 in range( x, x + size_of_block):
					if y1 < height and x1 < width:
						send_to_svm.append(hog[y1][x1])##creating a block of cells. each cells hold size_of_cell*size_of_cell of gradients.
##			found = check_for_faces(send_to_svm)##send the block to a learning machine that was trained to locate faces			
			if found:##if found - draw a bounding box around the face
				loc_x = int(x * size_of_cell)
				loc_y = int(y * size_of_cell)
				size = int(size_of_cell * size_of_block)
				for x1 in range(loc_x,loc_x+size):##creating rectangle
					im.putpixel((x1,loc_y),(255,0,0))
				for y1 in range(loc_y,loc_y+size):
					im.putpixel((loc_x,y1),(255,0,0))
				for x1 in range(loc_x,loc_x+size):
					im.putpixel((x1,loc_y+size),(255,0,0))
				for y1 in range(loc_y,loc_y+size):
					im.putpixel((loc_x+size,y1),(255,0,0))
				found = False
	if pyramid > 0:##recursively repeat this function - get the picture smaller each time
		im = im.resize((int(img_width/2), int(img_height/2)),Image.ANTIALIAS)
		gr = find_gradients(im,int(img_width/2),int(img_height/2))
		h = create_hog_feature(gr,int(img_width/2),int(img_height/2))
		nwidth = len(h)
		nheight = len(h[0])
		list_of_img = list_of_img + SVM(im,h,nwidth,nheight,size_of_cell,size_of_block,pyramid-1)
	return list_of_img
"""
divide the picture into cells
each cell holds a couple of gradients(magnitude+orientation)
"""
def create_hog_feature(gradient,width,height,size_of_cell=8):
	hog = []
	cell_row = []
	for y in range(0,height,size_of_cell):
		for x in range(0,width,size_of_cell):
			g = []
			d = []
			for y1 in range(0,size_of_cell):
				for x1 in range(0,size_of_cell):
					g.append(gradient[x+x1][y+y1][0])
					d.append(gradient[x+x1][y+y1][1]+ 90)
			cell = []
			cell.append(0)
			cell.append(0)
			cell.append(0)
			cell.append(0)
			cell.append(0)
			cell.append(0)
			cell.append(0)
			cell.append(0)
			cell.append(0)
			for i in range(0,len(g)):
				if d[i] >= 0 and d[i] <= 20:
					cell[1] = (g[i] * ((d[i])/20.0)) + cell[0]
					cell[2] = (g[i] * ((20-d[i])/20.0)) + cell[1]
				if d[i] >= 20 and d[i] <= 40:
					cell[1] = (g[i] * ((d[i] - 20)/40.0)) + cell[1]
					cell[2] = (g[i] * ((40-d[i])/40.0)) + cell[2]
				if d[i] >= 40 and d[i] <= 60:
					cell[2] = (g[i] * ((d[i] - 40)/60.0)) + cell[2]
					cell[3] = (g[i] * ((60-d[i])/60.0)) + cell[3]
				if d[i] >= 60 and d[i] <= 80:
					cell[3] = (g[i] * ((d[i] - 60)/80.0)) + cell[3]
					cell[4] = (g[i] * ((80-d[i])/80.0)) + cell[4]
				if d[i] >= 80 and d[i] <= 100:
					cell[4] = (g[i] * ((d[i] - 80)/100.0)) + cell[4]
					cell[5] = (g[i] * ((100-d[i])/100.0)) + cell[5]
				if d[i] >= 100 and d[i] <= 120:
					cell[5] = (g[i] * ((d[i] - 100)/120.0)) + cell[5]
					cell[6] = (g[i] * ((120-d[i])/120.0)) + cell[6]
				if d[i] >= 120 and d[i] <= 140:
					cell[6] = (g[i] * ((d[i] - 120)/140.0)) + cell[6]
					cell[7] = (g[i] * ((140-d[i])/140.0)) + cell[7]
				if d[i] >= 140 and d[i] <= 160:
					cell[7] = (g[i] * ((d[i] - 140)/160.0)) + cell[7]
					cell[8] = (g[i] * ((160-d[i])/160.0)) + cell[8]
				if d[i] >= 160 and d[i] <= 180:
					cell[8] = g[i] + cell[8]
			cell_row.append(g)
		hog.append(cell_row)
		cell_row = []
	return hog

"""
find the gradient of each pixel
store all of the gradients(magnitude+orientation) in a list
"""
def find_gradients(im,width,height):
	x1 = x2 = y1 = y2 = 0
	g = []
	for y in range(0,height):
		xyd = []
		for x in range(0,width):
			if(x+1<width):
				x1,_,_ = im.getpixel((x + 1,y))
			if(x-1>0):
				x2,_,_ = im.getpixel((x - 1,y))
			if(y-1>0):
				y2,_,_ = im.getpixel((x,y - 1))
			if(y+1<height):
				y1,_,_ = im.getpixel((x,y + 1))
			X = x1-x2
			Y = y1-y2
			xy = int(((Y**2)+(X**2))**0.5)##calculating magnitude
			d = float((math.atan2(Y,X)*180))/(math.pi)##calculating orientation - atan2 returns radians - turning it to degrees by multipling it by 180 and then divide it by PI 
			"""pos = ""
			if (d <= 90 and d > 67.5) or ( d >= -90 and d < -67.5):
				pos = "u"
				if Y > 0:
					pos = "d"
			if d <= 67.5 and d > 22.5:
				pos = "ur"
				if X < 0 and Y < 0:
					pos = "dl"
			if d <= 22.5 and d > -22.5:
				pos = 'r'
				if X < 0:
					pos = 'l'
			if d <= -22.5 and d > 67.5:
				pos = 'ul'
				if X > 0 and Y > 0:
					pos = 'dr'"""
			xyd.append((xy,d))
		g.append(xyd)
	return g
"""
edge detector
"""
def edge_finder(im,im1,im2,im3,width,height):
	x1 = x2 = y1 = y2 = 0
	hog = []
	for y in range(0,height):
		xya = []
		for x in range(0,width):
			if(x+1<width):
				x1,_,_ = im.getpixel((x + 1,y))
			if(x-1>0):
				x2,_,_ = im.getpixel((x - 1,y))
			if(y-1>0):
				y2,_,_ = im.getpixel((x,y - 1))
			if(y+1<height):
				y1,_,_ = im.getpixel((x,y + 1))
			X = x1-x2 
			Y = y1-y2 
			d = (math.atan2(Y,X)*180)/(math.pi)
			#if X < 0:
			#	X = X * -1
			#if Y < 0:
			#	Y = Y * -1
			t1 = (X*90,X*90,X*90)
			t2 = (Y*90,Y*90,Y*90)
			xy = int(((Y**2)+(X**2))**0.5)
			xya.append((xy,d))
			t3 = (int(xy*d),int(xy*d),int(xy*d))
			im1.putpixel((x,y),t1)
			im2.putpixel((x,y),t2)
			im3.putpixel((x,y),t3)
		hog.append(xya)
	return hog
im = Image.open('truck.jpg','r')
im1 = Image.open('truck.jpg','r')
im2 = Image.open('truck.jpg','r')
im3 = Image.open('truck.jpg','r')
size = (256,256)
#im1 = im1.resize(size, Image.ANTIALIAS)
#im2 = im2.resize(size, Image.ANTIALIAS)
#im3 = im3.resize(size, Image.ANTIALIAS)
width, height = im.size
to_grey(im,width,height)
im = im.resize(size, Image.ANTIALIAS)
width, height = im.size
gradient = find_gradients(im,width,height)
hog = create_hog_feature(gradient,width,height)
width = len(hog)
height = len(hog[0])
listt = SVM(im,hog,width,height)
print("1")
#a = []
#z = []
#cnt = 0
#for x in gradient:
#	a = []
#	for y in x:
#		a.append(y[0])
#	z.append(a)


#print(len(create_hog_feature(gradient,width,height)),len(gradient))
"""edge_finder(im,im1,im2,im3,width,height)
plt.imshow(im3)#, cmap = plt.cm.binary)
plt.show()
im.show()
im1.show()
im2.show()
im3.show()


	"""
for imgg in listt:
	imgg.show()