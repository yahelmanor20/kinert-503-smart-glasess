import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import time
from skimage import data, color, feature
from sklearn.feature_extraction.image import PatchExtractor
from skimage import transform
import skimage
from sklearn.datasets import fetch_lfw_people
import joblib

from itertools import chain

import bens_hog


def fetch_positive_objects():
    faces = fetch_lfw_people(download_if_missing=True)
    positive_patches = faces.images
    print(positive_patches.shape)  # excepted result == (13233, 62, 47)
    return positive_patches


def fetch_negative_objects():
    imgs_to_use = ['camera', 'text', 'coins', 'moon',
                   'page', 'clock', 'immunohistochemistry',
                   'chelsea', 'coffee', 'hubble_deep_field']
    images = [color.rgb2gray(getattr(data, name)()) for name in imgs_to_use]  # data = skimage.data
    return images


def extract_patches(img, N,  patch_size, scale=1.0):  # takes the size we need from the negative pics
    extracted_patch_size = tuple((scale * np.array(patch_size)).astype(int))
    extractor = PatchExtractor(patch_size=extracted_patch_size,
                               max_patches=N, random_state=0)
    patches = extractor.transform(img[np.newaxis])  # TO DO:
    if scale != 1:
        patches = np.array([transform.resize(patch, patch_size)
                            for patch in patches])
    return patches


def combine_pos_and_neg(positive_patches, negative_patches):
    X_train = np.array([feature.hog(im) for im in chain(positive_patches, negative_patches)])  # chain is a build in function for itertors connection

    # 0 = face(positive)
    # 1 = not face(negative)
    y_train = np.zeros(X_train.shape[0])  # sets the first part of the array to 0
    y_train[:positive_patches.shape[0]] = 1  # sets the second part of the array to 1
    # TO DO:
    return X_train, y_train


def train(X_train, y_train):
    t = time.time()
    from sklearn.metrics import mean_absolute_error as mae
    from sklearn.naive_bayes import GaussianNB
    from sklearn.model_selection import train_test_split
    from sklearn.svm import LinearSVC
    from sklearn.model_selection import GridSearchCV
    print(time.time() - t)

    # train_test_split(GaussianNB(), X_train, y_train)
    # accuracy = mae(X_train, y_train)
    print(X_train.shape, y_train.shape)
    grid = GridSearchCV(LinearSVC(), {'C': [1.0, 2.0, 4.0, 8.0]})
    t = time.time()
    grid.fit(X_train, y_train)
    print('grid fit time:', time.time() - t)
    print(grid.best_score_, grid.best_params_)
    model = grid.best_estimator_
    model.fit(X_train, y_train)

    return model


def sliding_window(img, patch_size,
                   istep=2, jstep=2, scale=1.0):
    Ni, Nj = (int(scale * s) for s in patch_size)
    for i in range(0, img.shape[0] - Ni, istep):
        for j in range(0, img.shape[1] - Ni, jstep):
            patch = img[i:i + Ni, j:j + Nj]
            if scale != 1:
                patch = transform.resize(patch, patch_size)
            yield (i, j), patch


def test(model, face_patch_size):
    test_image = skimage.data.astronaut()
    test_image = skimage.color.rgb2gray(test_image)
    test_image = skimage.transform.rescale(test_image, 0.5)
    test_image = test_image[:160, 40:180]

    # plt.imshow(test_image, cmap='gray')
    # plt.axis('off')

    indices, patches = zip(*sliding_window(test_image,face_patch_size ))
    patches_hog = np.array([feature.hog(patch) for patch in patches])
    print(patches_hog.shape)
    labels = model.predict(patches_hog)
    print(labels.sum())

    fig, ax = plt.subplots()
    ax.imshow(test_image, cmap='gray')
    ax.axis('off')

    Ni, Nj = face_patch_size
    indices = np.array(indices)

    for i, j in indices[labels == 1]:
        ax.add_patch(plt.Rectangle((j, i), Nj, Ni, edgecolor='red',
                                   alpha=0.3, lw=2, facecolor='none'))


def main():
    """part 1"""
    images = fetch_negative_objects()
    positive_patches = fetch_positive_objects()
    face_patch_size = positive_patches[0].shape
    print('faces size: ', positive_patches[0].shape)

    # extract the needed patch from the negative images
    negative_patches = np.vstack([extract_patches(im, 1000,  positive_patches[0].shape, scale) for im in images for scale in [0.5, 1.0, 2.0]])

    print(negative_patches.shape)  # excepted result == (30000, 62, 47)
    """
    fig, ax = plt.subplots(6, 10)

    for i, axi in enumerate(ax.flat):
        axi.imshow(negative_patches[500 * i], cmap='gray')
        axi.axis('off')
    """
    """part 2"""

    X_train , y_train = combine_pos_and_neg(positive_patches, negative_patches)
    print(X_train.shape)

    """part 3"""
    model = train(X_train, y_train)
    """part 4"""
    joblib.dump(model, 'model.pickle')  # saves the model
    """part 5"""
    test(model, face_patch_size)


if __name__ == '__main__':
    main()
